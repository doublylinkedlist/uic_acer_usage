workdir = "/projects/cme_santc/xyan11/"

import os, io, time


def list_files(startpath):
    all_file_groups = []
    for root, dirs, files in os.walk(startpath):
        prefix = root
        useful_files = []
        cur_files = os.listdir(root)
        useful_files = [os.path.join(root, f) for f in cur_files if f.endswith(".pbs") or (".e" in f and f.split(".e")[1].isdigit()) or (".o" in f and f.split(".o")[1].isdigit())]
        useful_files = [f for f in useful_files if time.localtime(os.path.getctime(os.path.join(root, f))).tm_year == 2021]
        if len(useful_files) >= 3:
            all_file_groups.append(useful_files)
    return all_file_groups


def read_core_numbers_from_pbs(fname):
    fstr = None
    with io.open(fname, "r", newline="\n") as f:
        fstr = f.read()
    flines = fstr.strip().split("\n")
    for line in flines:
        if "#PBS -l nodes" in line:
            return int(line.split("nodes=")[1].split(":ppn=")[0]) * int('#PBS -l nodes=2:ppn=24'.split(":ppn=")[1])

def read_walltime_from_pbs(fname):
    fstr = None
    with io.open(fname, "r", newline="\n") as f:
        fstr = f.read()
    flines = fstr.strip().split("\n")
    for line in flines:
        if "#PBS" in line and "walltime=" in line:
            h, m, s = line.split("walltime=")[1].split(":")
            return float(h) + (float(m)/60.) + (float(s)/3600.)
        
def startEndTime(fname):
    #https://stackoverflow.com/questions/27549217/ctime-atime-and-mtime-how-to-interpret-them
    return os.stat(fname).st_mtime, os.stat(fname).st_ctime, os.stat(fname).st_atime

def total_size_MB(Folderpath):
    size = 0
    for path, dirs, files in os.walk(Folderpath):
        for f in files:
            fp = os.path.join(path, f)
            size += os.path.getsize(fp)
    return size / 1024 / 1024



dirnames = [[os.path.join(workdir, x), 
             str(time.localtime(os.path.getctime(os.path.join(workdir, x))).tm_year)] \
            for x in os.listdir(workdir) \
            if (x.startswith("2020") or x.startswith("2021")) and \
            time.localtime(os.path.getctime(os.path.join(workdir, x))).tm_year in [2020, 2021]]

all_file_groups = []
for dirname, year in dirnames:
    all_file_groups = all_file_groups + list_files(dirname)


total_cpu_hours = 0

out_str = ""
for file_group in all_file_groups:
    print(os.path.dirname(file_group[0]))
    #out_str = out_str + os.path.dirname(file_group[0]) + ","
    ncores = 0
    walltime_hours = 0
    max_walltime = 0
    for f in file_group:
        if f.endswith(".pbs"):
            valid_utf8 = True
            try:
                read_str = None
                with io.open(f, "r", newline="\n") as _f:
                    read_str = _f.read()
            except:
                valid_utf8 = False
            if valid_utf8:
                ncores = read_core_numbers_from_pbs(f)
                max_walltime = read_walltime_from_pbs(f)
    
    efile_walltime = 0
    found_efile = False
    for f in file_group:
        if ".e" in f and f.split(".e")[1].isdigit():
            found_efile = True
            mtime1, ctime1, atime1 = startEndTime(f)
            #print(max([mtime1, ctime1, atime1]) - min([mtime1, ctime1, atime1]))
            if max([mtime1, ctime1, atime1]) - min([mtime1, ctime1, atime1]) < 5:
                #print("Invalid efile")
                found_efile = False
            else:
                if (max([mtime1, ctime1, atime1]) - min([mtime1, ctime1, atime1])) / 3600. > max_walltime:
                    efile_walltime = max_walltime + efile_walltime
                else:
                    efile_walltime = (max([mtime1, ctime1, atime1]) - min([mtime1, ctime1, atime1])) / 3600. + efile_walltime
                    
    ofile_walltime = 0
    found_ofile = False
    for f in file_group:
        if ".o" in os.path.basename(f):
            if os.path.basename(f).split(".o")[1].isdigit():
                found_ofile = True
                mtime1, ctime1, atime1 = startEndTime(f)
                if max([mtime1, ctime1, atime1]) - min([mtime1, ctime1, atime1]) < 5:
                    #print("Invalid ofile")
                    found_ofile = False
                else:
                    if (max([mtime1, ctime1, atime1]) - min([mtime1, ctime1, atime1])) / 3600. > max_walltime:
                        ofile_walltime = max_walltime + ofile_walltime
                    else:
                        ofile_walltime = (max([mtime1, ctime1, atime1]) - min([mtime1, ctime1, atime1])) / 3600. + ofile_walltime
            
    walltime_hours = min([efile_walltime, ofile_walltime])
    if found_efile == False:
        #print("efile not found")
        if found_ofile == False:
            #print("ofile not found")
            #print(file_group[0])
            #print(os.path.dirname(file_group[0]))
            if total_size_MB(os.path.dirname(file_group[0])) > 10:
                print(max_walltime)
                walltime_hours = max_walltime + walltime_hours
            
    print("walltime hours:", walltime_hours, ", ncores: ", ncores)
    total_cpu_hours = walltime_hours * ncores + total_cpu_hours
    print(walltime_hours * ncores, "cpu hours")
    print("\n")
    out_str = out_str + os.path.dirname(file_group[0]) + "," + str(walltime_hours) + "," + str(ncores) + "," + str(walltime_hours * ncores) + "\n"
out_str = '"dirname","walltime","cpu_count","cpu_hours"\n' + out_str
with io.open("2021_cpu_hours_xyan11.csv", "w", newline="\n") as outfile:
    outfile.write(out_str)
total_cpu_hours
